package com.test.profileapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Address {
	@ApiModelProperty(notes = "Street Line")
	private String streetLine;
	@ApiModelProperty(notes = "Suburb")
	private String suburb;
	@ApiModelProperty(notes = "State")
	private String state;
	@ApiModelProperty(notes = "Country")
	private String country;
	
	public String getStreetLine() {
		return streetLine;
	}
	
	public void setStreetLine(String streetLine) {
		this.streetLine = streetLine;
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
}
