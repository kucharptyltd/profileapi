package com.test.profileapi.crm;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.profileapi.dto.Profile;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CRMClientTest {
	@Autowired
	private CRMClient client;
	
	@Test
	public void testURL() {
		assertThat(client.getUrl(), equalTo("http://test"));
	}
	
	@Test
	public void testGetProfile() {
		Profile profile = client.get("123");
		assertThat(profile.getId(), equalTo("123"));
	}
	
	@Test
	public void testCreateProfile() {
		Profile profile = client.create(new Profile("123"));
		assertThat(profile.getId(), equalTo("123"));
	}
}
