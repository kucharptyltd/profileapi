package com.test.profileapi.crm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.test.profileapi.dto.Profile;

@Service
public class CRMClient {

	@Value("${crm.url}")
	private String url;
	
	public Profile get(String id) {
		return new Profile(id);
	}
	
	public Profile create(Profile profile) {
		return profile;
	}
	
	public Profile update(Profile profile) {
		return profile;
	}
	
	public void delete(String id) {
		
	}
	
	public String getUrl() {
		return url;
	}
}
