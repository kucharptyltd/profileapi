package com.test.profileapi.controller;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.profileapi.crm.CRMClient;
import com.test.profileapi.dto.Profile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Validated
@Api(value="Profile API")
public class ProfileAPIController {
	
	private final static Logger log = LoggerFactory.getLogger(ProfileAPIController.class);
	
	@Autowired 
	private CRMClient crmClient;
	
	private static final String VALIDATION_REGEX = "^\\d+";
	
	@ApiOperation(value = "Get profile by ID", response = Profile.class)
	@RequestMapping(value = "/profile/{id}", method = RequestMethod.GET, produces = "application/json")
	public Object read(@Valid @Pattern(regexp = VALIDATION_REGEX) @PathVariable(value="id", required=true) String id) {
		log.info("Invoking get profile with param {}", id);
		
		Profile result = crmClient.get(id);

		return result;
	}
	
	@ApiOperation(value = "Create profile", response = Profile.class)
	@RequestMapping(value = "/profile", method = RequestMethod.PUT, produces = "application/json")
	public Object create(@Valid @RequestBody Profile profile) {
		log.info("Invoking create profile with param {}", profile);
		
		Profile result = crmClient.create(profile);

		return result;
	}
	
	@ApiOperation(value = "Update profile", response = Profile.class)
	@RequestMapping(value = "/profile", method = RequestMethod.POST, produces = "application/json")
	public Object update(@Valid @RequestBody Profile profile) {
		log.info("Invoking update profile with param {}", profile);
		
		Profile result = crmClient.update(profile);

		return result;
	}
	
	@ApiOperation(value = "Delete profile", response = Void.class)
	@RequestMapping(value = "/profile/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void delete(@Valid @Pattern(regexp = VALIDATION_REGEX) @PathVariable(value="id", required=true) String id) {
		log.info("Invoking delete profile with param {}", id);
		
		crmClient.delete(id);
	}

}
