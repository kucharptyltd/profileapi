package com.test.profileapi.controller;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.jsonpath.JsonPath;
import com.test.profileapi.dto.Profile;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProfileAPIControllerTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testGetProfileOK() throws Exception {

		// Sending Request
		ResponseEntity<String> response =
				restTemplate.getForEntity("/profile/{id}", String.class, "123");

		// Assertions
		String body = response.getBody();
		assertThat("Id is correct", JsonPath.read(body, "$.id"), equalTo("123"));
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
	
	@Test
	public void testGetProfileBadRequest() throws Exception {

		// Sending Request
		ResponseEntity<String> response =
				restTemplate.getForEntity("/profile/{id}", String.class, "sss");

		// Assertions
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
	}
	
	@Test
	public void testPostProfileOK() throws Exception {

		// Sending Request
		ResponseEntity<Profile> response =
				restTemplate.postForEntity("/profile", new Profile("123"), Profile.class);

		// Assertions
		Profile body = response.getBody();
		assertThat("Id is correct", body.getId(), equalTo("123"));
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
	
	@Test
	public void testPostProfileBadRequest() throws Exception {

		// Sending Request
		ResponseEntity<Profile> response =
				restTemplate.postForEntity("/profile", new Profile(), Profile.class);

		// Assertions
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
	}
	
	@Test
	public void testPutProfileOK() throws Exception {

		// Sending Request
		restTemplate.put("/profile", new Profile("123"));
		ResponseEntity<String> response =
				restTemplate.getForEntity("/profile/{id}", String.class, "123");

		// Assertions
		String body = response.getBody();
		assertThat("Title is correct", JsonPath.read(body, "$.id"), equalTo("123"));
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
	
	@Test
	public void testPutProfileBadRequest() throws Exception {

		// Sending Request
		restTemplate.put("/profile", null);

		ResponseEntity<String> response =
				restTemplate.getForEntity("/profile/", String.class);

		// Assertions
		assertThat(response.getStatusCode(), equalTo(HttpStatus.METHOD_NOT_ALLOWED));
	}
	
	@Test
	public void testDeleteProfileBadRequest() throws Exception {

		// Sending Request
		restTemplate.delete("/profile/");

		ResponseEntity<String> response =
				restTemplate.getForEntity("/profile/", String.class);

		// Assertions
		assertThat(response.getStatusCode(), equalTo(HttpStatus.METHOD_NOT_ALLOWED));
	}
}
