profile-api
=================

It a repository for Profile API to do CRUD operations on customer profiles

Setup
-----

Setup Eclipse project
* `./gradlew eclipse`

Build
* `./gradlew build`

Run
* `./gradlew bootRun` or `java -jar profile-api-1.0.0.jar`

Run tests
* `./gradlew test`


Swagger Documentation
---------------------
* At `http://localhost:8080/swagger-ui.html`
