package com.test.profileapi.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Profile {
	@ApiModelProperty(notes = "ID")
	@NotNull
	private String id;
	@ApiModelProperty(notes = "First Name")
	private String firstName;
	@ApiModelProperty(notes = "Last Name")
	private String lastName;
	@ApiModelProperty(notes = "Date of Birth")
	private String dob;
	@ApiModelProperty(notes = "Home Address")
	private Address homeAddress;
	@ApiModelProperty(notes = "Office Address")
	private Address officeAddress;
	@ApiModelProperty(notes = "Email")
	private String email;
	
	public Profile() {
		
	}
	
	public Profile(String id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getDob() {
		return dob;
	}
	
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public Address getHomeAddress() {
		return homeAddress;
	}
	
	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}
	
	public Address getOfficeAddress() {
		return officeAddress;
	}
	
	public void setOfficeAddress(Address officeAddress) {
		this.officeAddress = officeAddress;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob
		    + ", homeAddress=" + homeAddress + ", officeAddress=" + officeAddress + ", email=" + email + "]";
	}


}
